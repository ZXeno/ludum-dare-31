﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Utilities;

namespace LD31
{
    public class SFXManager : IDisposable
    {
        public static SFXManager Instance { get; private set; }

        private bool _playlistCheck = false;

        private Game1 _game;
        private ContentManager _content;
        private Random _rnd;

        private Song _gameSong1;
        private Song _gameSong2;
        private Song _failureSong;
        private Song _menuSong;

        private int _currentSong = 0;

        private SoundEffect _clickBlip1;
        private SoundEffect _clickBlip2;
        private SoundEffect _clickBlip3;
        private SoundEffect _clickBlip4;
        private SoundEffect _clickBlip5;
        private SoundEffect _clickBlip6;

        private SoundEffect _setdestblip1;
        private SoundEffect _setdestblip2;
        private SoundEffect _setdestblip3;
        private SoundEffect _setdestblip4;
        private SoundEffect _setdestblip5;

        private SoundEffect _xplode1;
        private SoundEffect _xplode2;
        private SoundEffect _xplode3;
        private SoundEffect _xplode4;
        private SoundEffect _xplode5;
        private SoundEffect _xplode6;
        private SoundEffect _xplode7;
        private SoundEffect _xplode8;
        private SoundEffect _xplode9;
        private SoundEffect _xplode10;

        private SoundEffect _buyUndock1;
        private SoundEffect _buyUndock2;

        private SoundEffect _sellUndock1;
        private SoundEffect _sellUndock2;

        private List<Song> _playlist;

        public Song GameSong1 { get { return _gameSong1; } }
        public Song GameSong2 { get { return _gameSong2; } }
        public Song MenuSong { get { return _menuSong; } }
        public Song FailureSong { get { return _failureSong; } }

        public SoundEffect ClickBlip1 { get { return _clickBlip1; } }
        public SoundEffect ClickBlip2 { get { return _clickBlip2; } }
        public SoundEffect ClickBlip3 { get { return _clickBlip3; } }

        public SoundEffect BuyUndock1 { get { return _buyUndock1; } }
        public SoundEffect BuyUndock2 { get { return _buyUndock2; } }

        public SoundEffect SellUndock1 { get { return _sellUndock1; } }
        public SoundEffect SellUndock2 { get { return _sellUndock2; } }

        public SFXManager(Game1 game)
        {
            _game = game;
            _content = game.Content;
            Instance = this;
            _rnd = new Random();
            _playlist = new List<Song>();

            MediaPlayer.Volume = .5f;
        }

        public void LoadContent()
        {
            _clickBlip1 = _content.Load<SoundEffect>("SoundFX/click_blip");
            _clickBlip2 = _content.Load<SoundEffect>("SoundFX/click_blip2");
            _clickBlip3 = _content.Load<SoundEffect>("SoundFX/click_blip3");
            _clickBlip4 = _content.Load<SoundEffect>("SoundFX/click_blip4");
            _clickBlip5 = _content.Load<SoundEffect>("SoundFX/click_blip5");
            _clickBlip6 = _content.Load<SoundEffect>("SoundFX/click_blip6");
            _buyUndock1 = _content.Load<SoundEffect>("SoundFX/buy_undock");
            _buyUndock2 = _content.Load<SoundEffect>("SoundFX/buy_undock2");
            _sellUndock1 = _content.Load<SoundEffect>("SoundFX/sell_undock");
            _sellUndock2 = _content.Load<SoundEffect>("SoundFX/sell_undock2");
            _setdestblip1 = _content.Load<SoundEffect>("SoundFX/set_dest0");
            _setdestblip2 = _content.Load<SoundEffect>("SoundFX/set_dest1");
            _setdestblip3 = _content.Load<SoundEffect>("SoundFX/set_dest2");
            _setdestblip4 = _content.Load<SoundEffect>("SoundFX/set_dest3");
            _setdestblip5 = _content.Load<SoundEffect>("SoundFX/set_dest4");
            _xplode1 = _content.Load<SoundEffect>("SoundFX/xplode0");
            _xplode2 = _content.Load<SoundEffect>("SoundFX/xplode1");
            _xplode3 = _content.Load<SoundEffect>("SoundFX/xplode2");
            _xplode4 = _content.Load<SoundEffect>("SoundFX/xplode3");
            _xplode5 = _content.Load<SoundEffect>("SoundFX/xplode4");
            _xplode6 = _content.Load<SoundEffect>("SoundFX/xplode5");
            _xplode7 = _content.Load<SoundEffect>("SoundFX/xplode6");
            _xplode8 = _content.Load<SoundEffect>("SoundFX/xplode7");
            _xplode9 = _content.Load<SoundEffect>("SoundFX/xplode8");
            _xplode10 = _content.Load<SoundEffect>("SoundFX/xplode9");

            _gameSong1 = _content.Load<Song>("SoundFX/Music/Artuir - Mercantile Solitude");
            _gameSong2 = _content.Load<Song>("SoundFX/Music/Artuir - Infinite Frontier");
            _menuSong = _content.Load<Song>("SoundFX/Music/MENU");
            _failureSong = _content.Load<Song>("SoundFX/Music/LOSE");


            _playlist.Add(_gameSong1);
            _playlist.Add(_gameSong2);
        }

        public void Update()
        {
            if(Input.GetKeyDown(Microsoft.Xna.Framework.Input.Keys.Down))
            {
                MediaPlayer.Volume -= 0.1f;
            }
            if (Input.GetKeyDown(Microsoft.Xna.Framework.Input.Keys.Up))
            {
                MediaPlayer.Volume += 0.1f;
            }

            if (Input.GetKeyDown(Microsoft.Xna.Framework.Input.Keys.Right) && _game.CurrentGameState == GameState.Playing)
            {
                MediaPlayer.Stop();
            }

            if(Input.GetKeyDown(Microsoft.Xna.Framework.Input.Keys.NumPad0))
            {
                ToggleMusic();
            }

            if(MediaPlayer.State != MediaState.Playing && _game.CurrentGameState == GameState.Playing) 
            {  
                if(_playlistCheck) 
                {
                    _playlistCheck = false;  
                    _currentSong++;
                    if (_currentSong > _playlist.Count - 1)
                    {
                        _currentSong = 0;
                    }
                }
                MediaPlayer.Play(_playlist[_currentSong]);  
            }

            if (MediaPlayer.State == MediaState.Playing && _game.CurrentGameState == GameState.Playing) 
            {
                _playlistCheck = true;  
            }

            if (MediaPlayer.State == MediaState.Playing && _game.CurrentGameState == GameState.Loss)
            {
                if(MediaPlayer.Queue.ActiveSong != _failureSong)
                {
                    MediaPlayer.Stop();
                    MediaPlayer.Play(_failureSong);
                }
            }

            if (MediaPlayer.State == MediaState.Playing && _game.CurrentGameState == GameState.Menu)
            {
                if (MediaPlayer.Queue.ActiveSong != _menuSong)
                {
                    MediaPlayer.Stop();
                    MediaPlayer.Play(_menuSong);
                }
            }
        }

        public void PlayMusic(Song s)
        {
            if (MediaPlayer.State != MediaState.Playing)
            {
                MediaPlayer.Play(s);
            }
        }

        public void StopMusic()
        {
            MediaPlayer.Stop();
        }

        public void PlayEffect(SoundEffect sfx)
        {
            sfx.Play();
        }

        public void PlayRandomSetDest()
        {
            List<SoundEffect> setDestList = new List<SoundEffect>();
            setDestList.Add(_setdestblip1);
            setDestList.Add(_setdestblip2);
            setDestList.Add(_setdestblip3);
            setDestList.Add(_setdestblip4);
            setDestList.Add(_setdestblip5);

            PlayEffect(setDestList[_rnd.Next(setDestList.Count)]);

            setDestList.Clear();
        }

        public void PlayRandomBuyUndock()
        {
            List<SoundEffect> buyUndox = new List<SoundEffect>();
            buyUndox.Add(_buyUndock1);
            buyUndox.Add(_buyUndock2);

            PlayEffect(buyUndox[_rnd.Next(buyUndox.Count)]);

            buyUndox.Clear();
        }

        public void PlayRandomSellUndock()
        {
            List<SoundEffect> sellundox = new List<SoundEffect>();
            sellundox.Add(_sellUndock1);
            sellundox.Add(_sellUndock2);

            PlayEffect(sellundox[_rnd.Next(sellundox.Count)]);

            sellundox.Clear();
        }

        public void PlayRandomClick()
        {
            List<SoundEffect> clicks = new List<SoundEffect>();
            clicks.Add(_clickBlip1);
            clicks.Add(_clickBlip2);
            clicks.Add(_clickBlip3);
            clicks.Add(_clickBlip4);
            clicks.Add(_clickBlip5);
            clicks.Add(_clickBlip6);
            clicks.Add(_clickBlip1);
            clicks.Add(_clickBlip2);
            clicks.Add(_clickBlip3);
            clicks.Add(_clickBlip4);
            clicks.Add(_clickBlip5);
            clicks.Add(_clickBlip6);

            PlayEffect(clicks[_rnd.Next(clicks.Count)]);

            clicks.Clear();
        }

        public void PlayRandomExplosion()
        {
            List<SoundEffect> xplosions = new List<SoundEffect>();
            xplosions.Add(_xplode1);
            xplosions.Add(_xplode10);
            xplosions.Add(_xplode2);
            xplosions.Add(_xplode3);
            xplosions.Add(_xplode4);
            xplosions.Add(_xplode5);
            xplosions.Add(_xplode6);
            xplosions.Add(_xplode7);
            xplosions.Add(_xplode8);
            xplosions.Add(_xplode9);

            PlayEffect(xplosions[_rnd.Next(xplosions.Count)]);

            xplosions.Clear();
        }

        public void Dispose()
        {
            _game = null;
            _rnd = null;
            _gameSong1.Dispose();
            _gameSong2.Dispose();
            _failureSong.Dispose();
            _clickBlip1.Dispose();
            _clickBlip2.Dispose();
            _clickBlip3.Dispose();
            _clickBlip4.Dispose();
            _clickBlip5.Dispose();
            _clickBlip6.Dispose();
            _buyUndock1.Dispose();
            _buyUndock2.Dispose();
            _sellUndock1.Dispose();
            _sellUndock2.Dispose();
        }

        private void ToggleMusic()
        {
            if(MediaPlayer.Volume == 1)
            {
                MediaPlayer.Volume = 0;
            }
            else
            {
                MediaPlayer.Volume = 1;
            }
        }
    }
}
