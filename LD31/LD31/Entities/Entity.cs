﻿using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace LD31
{
    public abstract class Entity
    {
        protected string _name;
        private Texture2D _sprite;
        private Point _mapPos;
        private bool _blocksTile;
        private bool _isEnabled;

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public Texture2D Sprite
        {
            get { return _sprite; }
            set { _sprite = value; }
        }

        public Point MapPosition
        {
            get { return _mapPos; }
            set { _mapPos = value; }
        }

        public Point ScreenPosition
        {
            get { return new Point(_mapPos.X * _sprite.Width, _mapPos.Y * _sprite.Height); }
        }

        public Rectangle Bounds
        {
            get { return new Rectangle(ScreenPosition.X, ScreenPosition.Y, _sprite.Width, _sprite.Height); }
        }

        public bool BlocksTile
        {
            get { return _blocksTile; }
            set { _blocksTile = value; }
        }

        public bool IsEnabled 
        { 
            get { return _isEnabled; }
            set { _isEnabled = value; }
        }

        public Entity(string name, Texture2D sprite, Point mappos, bool blockstile)
        {
            _name = name;
            _sprite = sprite;
            _mapPos = mappos;
            _blocksTile = blockstile;
            _isEnabled = true;
            Map.Instance.AllEntities.Add(this);
        }

        public virtual void Update(GameTime gameTime) { }
        public abstract void HandleInput();
        public abstract void TickUpdate();

        public bool HitTest()
        {
            if (Bounds.Contains(Input.MousePosition))
            {
                return true;
            }

            return false;
        }
    }
}
