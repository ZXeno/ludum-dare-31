﻿using System;
using Microsoft.Xna.Framework;

namespace LD31
{
    interface IControl
    {
        void Update();
        void Draw();
        void HandleInput();
        void SetPosition(Point p);
    }
}
