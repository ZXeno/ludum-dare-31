﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace LD31
{
    internal class Button : IControl
    {
        private Game1 _game;

        public Texture2D Image;
        public Label Label;
        public Point Position;
        public Point TextPosition = new Point(0, 0);
        public Action<Button> D;
        public Color HoverColor;
        public Color DefaultColor;
        private Color _currentColor;

        public Rectangle Bounds
        {
            get
            {
                return new Rectangle(
                    Position.X,
                    Position.Y,
                    Image.Width,
                    Image.Height
                    );
            }
        }

        public Button(Game1 game, Texture2D image, Point position, SpriteFont font)
        {
            _game = game;
            Image = image;
            Position = position;
            DefaultColor = Color.White;
            HoverColor = Color.White;
            _currentColor = DefaultColor;
            Label = new Label(_game, font,"",position);
        }

        public void SetText(string txt)
        {
            Label.Text = txt;
        }

        public void Update()
        {
            if(Bounds.Contains(Input.MousePosition))
            {
                _currentColor = HoverColor;
            }
            else
            {
                _currentColor = DefaultColor;
            }
            HandleInput();
        }

        public void HandleInput()
        {
            if (Input.GetButtonUp(0) && Bounds.Contains(Input.MousePosition))
            {
                if (D != null)
                {
                    D(this);
                }
            }
        }

        public void Draw()
        {
            _game.SpriteBatch.Draw(Image, Bounds, _currentColor);
            Label.Draw();
        }

        public void SetPosition(Point p)
        {
            Position = p;
        }
    }
}
