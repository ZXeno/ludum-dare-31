﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace LD31
{
    public class Map : DrawableGameComponent
    {
        public static Map Instance { get; private set; }

        public bool HasStarted { get; private set; }

        private Random _rnd;

        private Game1 _game;
        private ContentManager _content;
        private Player _player;

        private Texture2D _ship1;
        private Texture2D _yellowStar;
        private Texture2D _empty_E_Overlay;
        private Texture2D _emptyPix;
        private Texture2D _mouseHoverOverlay;
        private Texture2D _selectedShipSprite;
        private Texture2D _buy_b_overlay;
        private Texture2D _sell_s_overlay;
        private Texture2D _selectedStar;

        private Tile[,] _map;
        private Node[,] _pathMap;

        private double _prevTickTime = 0.0;
        private double _nextTickTime = .5;
        private double _tickTimer = 0;
        

        public int TileResolution { get { return _game.cTileResolution; } }
        public int MapSize { get { return _game.cMapSize; } }

        public List<Entity> AllEntities { get; set; }
        public List<Entity> EntitiesToRemove { get; private set; }

        public Tile[,] pMap { get { return _map; } }
        public Node[,] nMap { get { return _pathMap; } }

        public Map(Game1 game)
            : base(game)
        {
            _game = game;
            _content = _game.Content;
            HasStarted = false;
        }

        public override void Initialize()
        {
            _rnd = new Random();
            _map = new Tile[MapSize, MapSize];
            _pathMap = new Node[MapSize, MapSize];
            _player = new Player();

            AllEntities = new List<Entity>();
            EntitiesToRemove = new List<Entity>();
            Instance = this;            

            base.Initialize();
        }

        protected override void LoadContent()
        {
            _ship1 = _content.Load<Texture2D>("ship1");
            _yellowStar = _content.Load<Texture2D>("yellowstar2");
            _empty_E_Overlay = _content.Load<Texture2D>("emptyOverlay");
            _emptyPix = _content.Load<Texture2D>("empty");
            _mouseHoverOverlay = _content.Load<Texture2D>("hovertex");
            _selectedShipSprite = Game1.Instance.Content.Load<Texture2D>("selectedship");
            _buy_b_overlay = Game1.Instance.Content.Load<Texture2D>("buyOverlay");
            _sell_s_overlay = Game1.Instance.Content.Load<Texture2D>("sellOverlay");
            _selectedStar = Game1.Instance.Content.Load<Texture2D>("selectedStar");

            base.LoadContent();
        }

        public void StartGame()
        {
            if (!HasStarted)
            {
                
                int tileidcntr = 0;
                for (int y = 0; y < MapSize; y++)
                {
                    for (int x = 0; x < MapSize; x++)
                    {
                        Tile t = new Tile("Tile (" + x.ToString() + ", " + y.ToString() + ")", tileidcntr, _emptyPix, new Point(x, y), new Point(x * TileResolution, y * TileResolution));
                        Node n = new Node();
                        n.X = x;
                        n.Y = y;
                        t.Node = n;

                        _map[x, y] = t;
                        _pathMap[x, y] = n;

                        tileidcntr++;
                    }
                }

                int starcount = _rnd.Next(10, 30);
                bool consume = false;
                for (int s = 0; s < starcount; s++)
                {
                    Point rp = new Point(_rnd.Next(0, MapSize), _rnd.Next(0, MapSize));
                    Star nextStar = new Star("Star: " + s.ToString(), _yellowStar, _selectedStar, _buy_b_overlay, _sell_s_overlay, rp);
                    nextStar.IsConsumer = consume;
                    _map[rp.X, rp.Y].StarOnTile = nextStar;
                    if (!consume) { consume = true; }
                    else { consume = false; }
                    nextStar.SetTotalProducts(_rnd.Next(-10, 10));
                }

                CreateShip();

                HasStarted = true;
            }
        }

        public override void Update(GameTime gameTime)
        {
            if(EntitiesToRemove.Count > 0)
            {
                foreach (Entity e in EntitiesToRemove)
                {
                    AllEntities.Remove(e);
                }

                EntitiesToRemove.Clear();
            }

            if (_game.CurrentGameState == GameState.Playing && HasStarted)
            {
                _tickTimer += gameTime.ElapsedGameTime.TotalSeconds;

                foreach (Entity e in AllEntities)
                {
                    if (e.IsEnabled)
                    {
                        e.Update(gameTime);
                        e.HandleInput();
                    }
                }

                foreach (Tile t in _map)
                {
                    t.Update(gameTime);
                }

                if (_prevTickTime + _nextTickTime <= _tickTimer)
                {


                    foreach (Entity e in AllEntities)
                    {
                        if(e.IsEnabled)
                        e.TickUpdate();
                    }

                    _prevTickTime = _tickTimer;
                }
            }



            base.Update(gameTime);
        }


        public override void Draw(GameTime gameTime)
        {
            if (_game.CurrentGameState == GameState.Playing && HasStarted || _game.CurrentGameState == GameState.Loss && HasStarted)
            {
                for (int x = 0; x < MapSize; x++)
                {
                    for (int y = 0; y < MapSize; y++)
                    {
                        _game.SpriteBatch.Draw(_map[x, y].Sprite, _map[x, y].Bounds, Color.White);

                        if (_map[x, y].HitTest())
                        {
                            _game.SpriteBatch.Draw(_mouseHoverOverlay, new Rectangle(_map[x, y].ScreenPosition.X, _map[x, y].ScreenPosition.Y, TileResolution, TileResolution), Color.White);
                        }
                    }
                }

                foreach(Entity e in AllEntities)
                {
                    _game.SpriteBatch.Draw(e.Sprite, e.Bounds, Color.White);

                    if(e is Star)
                    {
                        Star s = e as Star;
                        _game.SpriteBatch.Draw(s.CurrentOverlay, e.Bounds, Color.White);
                    }                    
                }

                if(Player.Current.Money <= 0 || Player.Current.OwnedShips.Count == 0)
                {
                    _game.ChangeGameState(GameState.Loss);
                }
            }

            base.Draw(gameTime);
        }

        public Tile GetTileAtPosition(Point pos)
        {
            for (int y = 0; y < MapSize; y++)
            {
                for (int x = 0; x < MapSize; x++)
                {
                    if(_map[x,y].Bounds.Contains(pos))
                    {
                        return _map[x, y];
                    }
                }
            }

            return null;
        }

        public Tile GetTileAtPosition(int mapx, int mapy)
        {

            return _map[mapx, mapy];
        }

        public int GetNodeMapCost(int x, int y)
        {
            if((x < 0) || (x > MapSize - 1))
            {
                return -1;
            }

            if((y < 0) || (y > MapSize - 1))
            {
                return -1;
            }

            if(_map[x,y].IsOpen)
            {
                return 1;
            }
            else
            {
                return -1;
            }
        }

        public void CreateShip()
        {
            if (Player.Current.OwnedShips.Count > 0) { Player.Current.Money -= 5000; }
            Ship newShip = new Ship("Ship " + Player.Current.ShipIDCounter.ToString(), _ship1, _selectedShipSprite, new Point(30, 30));
            Player.Current.ShipIDCounter++;
        }

        public void RemoveEntity(Entity etr)
        {
            EntitiesToRemove.Add(etr);
        }

        public void ClearGameMap()
        {
            Player.Current.OwnedShips.Clear();
            Player.Current.OwnedStars.Clear();
            Player.Current.ShipsNeedingDestination.Clear();
            Player.Current.Money = 10000;
            Player.Current.ShipIDCounter = 0;
            Player.Current.SelectedEntity = null;
            Player.Current.SelectedTile = null;

            for (int y = 0; y < MapSize; y++)
            {
                for (int x = 0; x < MapSize; x++)
                {
                    _map[x, y] = null;
                    _pathMap[x, y] = null;
                }
            }

            AllEntities.Clear();
            EntitiesToRemove.Clear();

            _tickTimer = 0;
            _prevTickTime = 0;
            HasStarted = false;
        }
    }
}
