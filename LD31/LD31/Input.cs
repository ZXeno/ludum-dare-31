﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace LD31
{
    public class Input : GameComponent
    {
        private static KeyboardState _keyboardState;
        private static KeyboardState _lastKeyboardState;
        private static MouseState _mouseState;
        private static MouseState _lastMouseState;

        public static KeyboardState KeyboardState
        {
            get { return _keyboardState; }
        }
        public static KeyboardState LastKeyboardState
        {
            get { return _lastKeyboardState; }
        }
        public static MouseState MouseState
        {
            get { return _mouseState; }
        }
        public static MouseState LastMouseState
        {
            get { return _lastMouseState; }
        }

        public static Point MousePosition
        {
            get { return _mouseState.Position; }
        }

        public Input(Game1 game)
            : base(game)
        {

        }

        public override void Initialize()
        {
            _lastKeyboardState = _keyboardState;
            _lastMouseState = _mouseState;
            _keyboardState = Keyboard.GetState();
            _mouseState = Mouse.GetState();

            base.Initialize();
        }

        public override void Update(GameTime gameTime)
        {
            _lastKeyboardState = _keyboardState;
            _lastMouseState = _mouseState;
            _keyboardState = Keyboard.GetState();
            _mouseState = Mouse.GetState();

            base.Update(gameTime);
        }

        public static bool GetKeyUp(Keys key)
        {
            return _keyboardState.IsKeyUp(key) && _lastKeyboardState.IsKeyDown(key);
        }

        public static bool GetKeyDown(Keys key)
        {
            return _keyboardState.IsKeyDown(key) && _lastKeyboardState.IsKeyUp(key);
        }

        public static bool GetKey(Keys key)
        {
            return _keyboardState.IsKeyDown(key);
        }

        public static bool GetButtonDown(int button)
        {
            if (button > 4)
            {
                return false;
            }

            switch (button)
            {
                case 0:
                    return (_mouseState.LeftButton == ButtonState.Pressed) && (_lastMouseState.LeftButton == ButtonState.Released);
                case 1:
                    return (_mouseState.RightButton == ButtonState.Pressed) && (_lastMouseState.RightButton == ButtonState.Released);
                case 2:
                    return (_mouseState.MiddleButton == ButtonState.Pressed) && (_lastMouseState.MiddleButton == ButtonState.Released);
                case 3:
                    return (_mouseState.XButton1 == ButtonState.Pressed) && (_lastMouseState.XButton1 == ButtonState.Released);
                case 4:
                    return (_mouseState.XButton2 == ButtonState.Pressed) && (_lastMouseState.XButton2 == ButtonState.Released);
            }

            return false;
        }

        public static bool GetButtonUp(int button)
        {
            if (button > 4)
            {
                return false;
            }

            switch (button)
            {
                case 0:
                    return (_mouseState.LeftButton == ButtonState.Released) && (_lastMouseState.LeftButton == ButtonState.Pressed);
                case 1:
                    return (_mouseState.RightButton == ButtonState.Released) && (_lastMouseState.RightButton == ButtonState.Pressed);
                case 2:
                    return (_mouseState.MiddleButton == ButtonState.Released) && (_lastMouseState.MiddleButton == ButtonState.Pressed);
                case 3:
                    return (_mouseState.XButton1 == ButtonState.Released) && (_lastMouseState.XButton1 == ButtonState.Pressed);
                case 4:
                    return (_mouseState.XButton2 == ButtonState.Released) && (_lastMouseState.XButton2 == ButtonState.Pressed);
            }

            return false;
        }

        public static bool GetButton(int button)
        {
            if (button > 4)
            {
                return false;
            }

            switch (button)
            {
                case 0:
                    return _mouseState.LeftButton == ButtonState.Pressed;
                case 1:
                    return _mouseState.RightButton == ButtonState.Pressed;
                case 2:
                    return _mouseState.MiddleButton == ButtonState.Pressed;
                case 3:
                    return _mouseState.XButton1 == ButtonState.Pressed;
                case 4:
                    return _mouseState.XButton2 == ButtonState.Pressed;
            }

            return false;
        }
    }
}
