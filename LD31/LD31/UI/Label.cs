﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace LD31
{
    internal class Label : IControl
    {
        Game1 _game;

        public SpriteFont Font;
        public string Text = "";
        public Point Position;
        public Rectangle Bounds
        {
            get { return new Rectangle(Position.X, Position.Y, (int)Font.MeasureString(Text).X + 1, (int)Font.MeasureString(Text).Y + 1); }
        }
        

        public Label(Game1 game, SpriteFont font, string txt, Point position)
        {
            _game = game;
            Font = font;
            Text = txt;
            Position = position;
        }

        public void Update() 
        { 
            if(Bounds.X + Bounds.Width > _game.Graphics.PreferredBackBufferWidth)
            {
                Position.X -= (Bounds.X + Bounds.Width) - _game.Graphics.PreferredBackBufferWidth;
            }

            if (Bounds.Y + Bounds.Height > _game.Graphics.PreferredBackBufferHeight)
            {
                Position.X -= (Bounds.X + Bounds.Width) - _game.Graphics.PreferredBackBufferWidth;
            }
        }

        public void Draw()
        {
            _game.SpriteBatch.DrawString(Font, Text, Position.ToVector2(), Color.White);
        }

        public void HandleInput() { }

        public void SetPosition(Point p)
        {
            Position = p;
        }
    }
}
