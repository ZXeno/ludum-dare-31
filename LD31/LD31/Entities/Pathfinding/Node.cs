﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LD31
{
    public class Node : IComparable
    {
        public double F;
        public double G;
        public double H;  // f = G + heuristic
        public int X;
        public int Y;
        public int Cost = 1;
        public Node parent;
        public List<Node> _neighbors;
        public bool Visited = false;

        public Node() 
        {
            _neighbors = new List<Node>();
        }

        public void FindNeighbors()
        {
            sbyte[,] direction = new sbyte[8, 2] { { 0, -1 }, { 1, 0 }, { 0, 1 }, { -1, 0 }, { 1, -1 }, { 1, 1 }, { -1, 1 }, { -1, -1 } };

            for (int i = 0; i < 8; i++)
            {
                int x = X + direction[i, 0];
                int y = Y + direction[i, 1];

                if (x < 0 || y < 0 || x >= Map.Instance.MapSize || y >= Map.Instance.MapSize)
                {
                    continue;
                }

                _neighbors.Add(Map.Instance.nMap[x, y]);
            }
        }

        public int CompareTo(object obj)
        {
            Node nobj = (Node)obj;

            if (F > nobj.F) { return 1; }
            else if (F < nobj.F) { return -1; }
            else { return 0; }
        }
    }
}
