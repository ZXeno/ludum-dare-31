﻿using System;
using System.Collections;
using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace LD31
{
    public class Astar
    {
        Node[,] _map;

        public Astar(Node[,] navmap)
        {
            _map = navmap;

            foreach (Node n in _map)
            {
                n.parent = null;
                n.Visited = false;
                n.G = 0;
                n.H = 0;
                n.Cost = 1;
                n._neighbors.Clear();
                n.FindNeighbors();
            }
        }

        public Stack<Point> AstarPath(Tile source, Tile target)
        {
            PriorityQueueB<Node> _open = new PriorityQueueB<Node>();
            Stack<Point> _path = new Stack<Point>();
            _path.Clear();
            _open.Clear();

            Node start = _map[source.Node.X, source.Node.Y];
            Node end = _map[target.Node.X, target.Node.Y];

            start.G = 0;
            start.H = Heuristics(new Point(start.X, start.Y), new Point(end.X,end.Y));
            start.F = start.G + start.H;

            _open.Push(start);

            while (_open.Count > 0)
            {
                Node current = _open.Pop();
               
                if(current.X == end.X && current.Y == end.Y)
                {
                    while(current.parent != null)
                    {
                        _path.Push(new Point(current.X, current.Y));
                        current = current.parent;
                    }
                    CleanNodeMap();
                    _open.Clear();
                    break;
                }

                current.Visited = true;

                foreach (Node nbr in current._neighbors)
                {
                    if(nbr.Visited)
                    {
                        continue;
                    }

                    double newg = current.G + nbr.Cost;

                    bool beenvisited = nbr.Visited;

                    if (!beenvisited || newg < nbr.G)
                    {
                        nbr.Visited = true;
                        nbr.parent = current;
                        nbr.H = Heuristics(new Point(nbr.X, nbr.Y), new Point(end.X, end.Y));
                        nbr.G = newg;
                        nbr.F = nbr.G + nbr.H;
                        if(!beenvisited)
                        {
                            _open.Push(nbr);
                        }
                    }
                }
            }

            return _path;
		}

        private double Heuristics(Point pos0, Point pos1)
        {
            var D = 1;
            var D2 = Math.Sqrt(2);
            var d1 = Math.Abs(pos1.X - pos0.X);
            var d2 = Math.Abs(pos1.Y - pos0.Y);
            return (D * (d1 + d2)) + ((D2 - (2 * D)) * Math.Min(d1,d2));
        }

        private void CleanNodeMap()
        {
            foreach (Node n in _map)
            {
                n.parent = null;
                n.Visited = false;
                n.G = 0;
                n.H = 0;
                n.Cost = 1;
                n._neighbors.Clear();
                n.FindNeighbors();
            }
        }
    }
}

