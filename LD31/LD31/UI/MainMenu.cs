﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace LD31
{
    internal class MainMenu
    {
        private Game1 _game;
        private ContentManager _content;

        private Texture2D _buttonBG;
        private SpriteFont _titfont;
        private SpriteFont _uifont;

        private Label _titleLabel;
        private Button _startButton;
        private Button _exitButton;
        private Label _volumeLabel;

        private List<IControl> _controls;

        public MainMenu(Game1 game)
        {
            _game = game;
            _content = _game.Content;
            _controls = new List<IControl>();
        }

        public void LoadContent()
        {
            _buttonBG = _content.Load<Texture2D>("menu_button_bg");
            _titfont = _content.Load<SpriteFont>("titlefont");
            _uifont = _content.Load<SpriteFont>("uifont");

            int lh = 26;

            _titleLabel = new Label(_game, _titfont, "LUDUM TRADERS!", new Point(_game.Graphics.PreferredBackBufferWidth / 2 - _buttonBG.Width / 2, 30));
            _startButton = new Button(_game, _buttonBG, new Point(_game.Graphics.PreferredBackBufferWidth / 2 - _buttonBG.Width / 2, _game.Graphics.PreferredBackBufferHeight / 2), _uifont);
            _exitButton = new Button(_game, _buttonBG, new Point(_game.Graphics.PreferredBackBufferWidth / 2 - _buttonBG.Width / 2, _game.Graphics.PreferredBackBufferHeight / 2 + lh), _uifont);
            _volumeLabel = new Label(_game, _uifont, "Volume: ", new Point(_game.Graphics.PreferredBackBufferWidth - 40, _game.Graphics.PreferredBackBufferHeight - 20));

            Action<Button> startgame = new Action<Button>(
                delegate 
                {
                    _game.Map.StartGame();
                    _game.ChangeGameState(GameState.Playing);
                    SFXManager.Instance.PlayEffect(SFXManager.Instance.ClickBlip1);
                    SFXManager.Instance.StopMusic();
                });

            Action<Button> exitgame = new Action<Button>(
                delegate
                {
                    SFXManager.Instance.PlayEffect(SFXManager.Instance.ClickBlip1);
                    _game.Exit();
                });

            _startButton.D = startgame;
            _exitButton.D = exitgame;

            _startButton.Label.Text = "Start Game";
            _exitButton.Label.Text = "Exit Game";

            _startButton.Label.SetPosition(new Point(Convert.ToInt32(_startButton.Position.X + (_startButton.Image.Width / 2) - (_startButton.Label.Font.MeasureString(_startButton.Label.Text).X / 2)), _startButton.Position.Y));
            _exitButton.Label.SetPosition(new Point(Convert.ToInt32(_exitButton.Position.X + (_exitButton.Image.Width / 2) - (_exitButton.Label.Font.MeasureString(_exitButton.Label.Text).X / 2)), _exitButton.Position.Y));

            _controls.Add(_titleLabel);
            _controls.Add(_startButton);
            _controls.Add(_exitButton);
            _controls.Add(_volumeLabel);
        }

        public void Update()
        {
            if (_game.CurrentGameState == GameState.Menu)
            {
                _volumeLabel.Text = "Volume: " + (Math.Round(MediaPlayer.Volume, 2) * 100).ToString();

                foreach (IControl c in _controls)
                {
                    c.HandleInput();
                    c.Update();
                }

                _titleLabel.SetPosition(new Point(_game.Graphics.PreferredBackBufferWidth / 2 - Convert.ToInt32(_titleLabel.Font.MeasureString(_titleLabel.Text).X / 2), _game.Graphics.PreferredBackBufferHeight / 4));
            }
        }

        public void Draw()
        {
            if (_game.CurrentGameState == GameState.Menu)
            {
                foreach (IControl c in _controls)
                {
                    c.Draw();
                }
            }
        }
    }
}
