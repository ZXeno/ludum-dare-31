﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.Collections.Generic;

namespace LD31
{
    public class Star : Entity
    {
        private Texture2D _buyOverlay;
        private Texture2D _sellOverlay;
        private Texture2D _selected;
        private Texture2D _default;

        private int _maxCargoValue = 250;
        private int _fuelCost = 5;
        private bool _consumer = false;
        private int _consumeRate = 1;
        private int _produceRate = 1;
        private int _baseExchangeRate = 100;
        private int _currExchangeRate = 100;
        private int _dockTime = 3;
        private int _totalProduct = 0;

        private double _productionticker = 0;
        private double _nextProductionTick = 5;
        private double _prevProductionTick = 0;

        public double ProductionTicker { get { return _productionticker; } }

        public Dictionary<Ship, int> ShipsInSystem;
        public Texture2D CurrentOverlay { get; protected set; }
        public int TotalProduct { get { return _totalProduct; } }
        public int CurrentExchangeRate { get { return _currExchangeRate; } }
        public bool IsConsumer 
        { 
            get { return _consumer; } 
            set 
            { 
                _consumer = value;
                if (!value) { CurrentOverlay = _buyOverlay; }
                else { CurrentOverlay = _sellOverlay; }
            } 
        }

        public Star(string name, Texture2D sprite, Texture2D selected, Texture2D buyoverlay, Texture2D selloverlay, Point mappos)
            : base(name, sprite, mappos, true)
        {
            ShipsInSystem = new Dictionary<Ship, int>();
            _buyOverlay = buyoverlay;
            _sellOverlay = selloverlay;
            _default = sprite;
            _selected = selected;
            _name = name;
        }

        public override void Update(GameTime gameTime) 
        {
            // set the correct sprite if we're selected or not
            if (Player.Current.SelectedEntity == this)
            {
                if (Sprite != _selected)
                {
                    Sprite = _selected;
                }
            }
            else
            {
                if (Sprite != _default)
                {
                    Sprite = _default;
                }
            }

            // Check overlays
            if (!_consumer)
            {
                if (CurrentOverlay != _sellOverlay) // Buy icon
                {
                    CurrentOverlay = _sellOverlay;
                }
            }
            else
            {
                if (CurrentOverlay != _buyOverlay) // sell icon
                {
                    CurrentOverlay = _buyOverlay;
                }
            }

            // Production
            _productionticker += gameTime.ElapsedGameTime.TotalSeconds;
            
            if(_productionticker >= _prevProductionTick + _nextProductionTick)
            {
                if(!_consumer)
                {
                    _totalProduct += _produceRate;
                }
                else
                {
                    _totalProduct -= _consumeRate;
                }

                _prevProductionTick = _productionticker;

                // Don't let the timer break
                if (_productionticker > 100000)
                {
                    ResetTimer();
                }
            }

            MathHelper.Clamp(_totalProduct, -50, 250);
        }


        // Tick update!
        public override void TickUpdate()
        {

            // Adjust prices based on quantitiy of supplies.
            if(_totalProduct < 0)
            {
                _currExchangeRate = _baseExchangeRate + MathHelper.Clamp(Math.Abs((_totalProduct / 2) * _baseExchangeRate), _baseExchangeRate, _maxCargoValue);
            }
            if(_totalProduct > 0)
            {
                _currExchangeRate = MathHelper.Clamp((_baseExchangeRate - _totalProduct), 50, _maxCargoValue);
            }

            


            // get a key list of docked ships
            List<Ship> shipkey = new List<Ship>(ShipsInSystem.Keys);

            // Process docked ships
            foreach(Ship s in shipkey)
            {
                // decrement the dock timer counter
                ShipsInSystem[s]--;

                // if the counter is at 0...
                if(ShipsInSystem[s] == 0)
                {
                    // refuel ship, provided player can afford it
                    if (Player.Current.Money >= _fuelCost * (s.MaxFuel - s.Fuel))
                    {
                        Player.Current.Money -= _fuelCost * (s.MaxFuel - s.Fuel);
                        s.Refuel(s.MaxFuel - s.Fuel);
                    }

                    // if the system isn't a consumer, buy stuff
                    if (!_consumer)
                    {
                        int cargochange = s.AvailableCargoSpace;
                        if (Player.Current.Money >= _currExchangeRate * cargochange)
                        {
                            Player.Current.Money -= _currExchangeRate * cargochange;
                            s.CurrentCargo += cargochange;
                        }
                    }
                    else // sell stuff
                    {
                        int cargochange = s.CurrentCargo;
                        Player.Current.Money += _currExchangeRate * cargochange;
                        s.CurrentCargo -= cargochange;
                    }

                    s.Undock();
                    ShipsInSystem.Remove(s);
                }
            }
        }


        public void DockShip(Ship s)
        {
            ShipsInSystem.Add(s, _dockTime);
        }

        public override void HandleInput()
        {
            if(Input.GetButtonDown(0) && HitTest())
            {
                if(ShipsInSystem.Count == 0)
                {
                    Player.Current.SelectedEntity = this;
                }

                else
                {
                    List<Ship> shipkey = new List<Ship>(ShipsInSystem.Keys);

                    Player.Current.SelectedEntity = shipkey[0];
                }
            }
        }

        public void SetTotalProducts(int v)
        {
            _totalProduct = v;
        }

        private void ResetTimer()
        {
            _productionticker = 0;
            _prevProductionTick = 0;
        }
    }
}
