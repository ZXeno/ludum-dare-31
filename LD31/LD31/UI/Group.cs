﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace LD31
{

    public enum ButtonTextPosition { Left, Center, Right }

    internal class ListGroup : IControl
    {
        public Point Position;
        public int Width;
        public int Height;
        public int LineHeight = 20;
        public List<IControl> Controls { get; protected set; }
        public ButtonTextPosition ButtonTextAlignment { get; set; }
        public Rectangle Bounds
        {
            get { return new Rectangle(Position.X, Position.Y, Width, Height); }
        }


        public ListGroup(Point position, int width)
        {
            Position = position;
            Width = width;
            Controls = new List<IControl>();
            ButtonTextAlignment = ButtonTextPosition.Center;
        }

        public void Update()
        {
            Height = LineHeight * Controls.Count;
            foreach (IControl c in Controls)
            {
                c.Update();
                c.HandleInput();
            }
        }

        public void Draw()
        {
            foreach(IControl c in Controls)
            {
                c.Draw();
            }
        }

        public void HandleInput()
        {

        }

        public void AddControl(IControl control)
        {
            Controls.Add(control);
            Reposition();
        }

        public void Reposition()
        {
            foreach (IControl c in Controls)
            {
                c.SetPosition(Point.Zero);
            }

            int index = 0;
            for (int i = 0; i < Controls.Count; i++)
            {
                IControl c = Controls[i];
                c.SetPosition(new Point(this.Position.X, this.Position.Y + LineHeight * index));

                if(c is Button && ButtonTextAlignment == ButtonTextPosition.Right)
                {
                    Button b = c as Button;
                    b.Label.SetPosition(new Point(b.Position.X + b.Image.Width + 4, b.Position.Y));
                }

                if (c is Button && ButtonTextAlignment == ButtonTextPosition.Center)
                {
                    Button b = c as Button;
                    b.Label.SetPosition(new Point(Convert.ToInt32(b.Position.X + (b.Image.Width / 2) - (b.Label.Font.MeasureString(b.Label.Text).X / 2)), b.Position.Y));
                }

                index++;
            }
        }

        public void Clear()
        {
            Controls.Clear();
        }

        public void SetPosition(Point p)
        {
            Position = p;
        }
    }
}
