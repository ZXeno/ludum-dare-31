﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace LD31
{
    public class Player
    {
        public static Player Current { get; private set; }

        private Tile _selectedTile;
        private Entity _selectedEntity;
        public Tile SelectedTile
        {
            get { return _selectedTile; }
            set { _selectedTile = value; }
        }
        public Entity SelectedEntity
        {
            get { return _selectedEntity; }
            set 
            { 
                _selectedEntity = value;
                SFXManager.Instance.PlayRandomClick();
            }
        }

        private List<Star> _ownedStars;
        public List<Star> OwnedStars
        {
            get { return _ownedStars; }
            set { _ownedStars = value; }
        }

        private List<Ship> _ownedShips;
        public List<Ship> OwnedShips 
        {
            get { return _ownedShips; }
            private set { _ownedShips = value; }
        }

        private int _money;
        public int Money
        {
            get { return _money; }
            set { _money = value; }
        }

        private int _shipIDCounter = 0;
        public int ShipIDCounter 
        { 
            get { return _shipIDCounter; }
            set { _shipIDCounter = value; }
        }
        public List<Ship> ShipsNeedingDestination { get; set; }

        public Player()
        {
            _ownedStars = new List<Star>();
            _ownedShips = new List<Ship>();
            ShipsNeedingDestination = new List<Ship>();
            Current = this;
            _money = 10000;
        }


    }
}
