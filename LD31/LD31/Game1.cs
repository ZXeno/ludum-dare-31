﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;

namespace LD31
{
    public enum GameState { Menu, Playing, Win, Loss, Pause }

    public class Game1 : Game
    {
        public static Game1 Instance;

        private Input _input;
        private UIManager _ui;
        private SFXManager _audio;
        private const int _cTileResolution = 16;
        private const int _cGameViewResolution = 960;
        private const int _cMapSize = 60;
        private const int _cUIWdith = 256;

        public GraphicsDeviceManager Graphics { get; protected set; }
        public SpriteBatch SpriteBatch { get; protected set; }

        public int cTileResolution { get { return _cTileResolution; } }
        public int cGameViewResolution { get { return _cGameViewResolution; } }
        public int cMapSize { get { return _cMapSize; } }
        public int cUIResolutionW { get { return _cUIWdith; } }
        public int cUIResolutionH { get { return Graphics.PreferredBackBufferHeight; } }
        public GameState CurrentGameState { get; private set; }

        protected Texture2D background;

        public Map Map { get; private set; }

        public Game1()
            : base()
        {
            Graphics = new GraphicsDeviceManager(this);
            Graphics.PreferredBackBufferHeight = _cGameViewResolution;
            Graphics.PreferredBackBufferWidth = _cGameViewResolution + _cUIWdith;
            Content.RootDirectory = "Content";

            _input = new Input(this);
            Instance = this;
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            CurrentGameState = GameState.Menu;

            Map = new Map(this);
            
            _ui = new UIManager(this);
            _audio = new SFXManager(this);
            IsMouseVisible = true;

            Components.Add(_input);
            Components.Add(Map);
            Components.Add(_ui);

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            SpriteBatch = new SpriteBatch(GraphicsDevice);

            background = Content.Load<Texture2D>("bg1");
            _audio.LoadContent();

            SFXManager.Instance.PlayMusic(SFXManager.Instance.MenuSong);
            base.LoadContent();
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            _input = null;
            _ui = null;
            _audio = null;
            Map = null;
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {

            if (Input.GetKeyDown(Keys.Escape))
            {
                switch(CurrentGameState)
                {
                    case GameState.Menu:
                        Exit();
                        break;
                    case GameState.Playing:
                        ChangeGameState(GameState.Pause);
                        break;
                    case GameState.Pause:
                        ChangeGameState(GameState.Playing);
                        break;
                }
            }

            _audio.Update();

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            SpriteBatch.Begin();
            SpriteBatch.Draw(background, new Rectangle(0, 0, background.Width, background.Height), Color.White);
            

            base.Draw(gameTime);
            SpriteBatch.End();
        }

        public void ChangeGameState(GameState state)
        {
            GameState prev = CurrentGameState;

            if (prev != GameState.Playing && state == GameState.Playing)
            {
                if (!Map.HasStarted)
                {
                    SFXManager.Instance.PlayMusic(SFXManager.Instance.GameSong1);
                    Map.StartGame();
                }
            }
            else
            {
                if (state != GameState.Loss)
                {
                    Map.ClearGameMap();
                    Map.StartGame();
                }
            }

            // Set current game state
            // some things require change before, and others after
            // the state change. 
            CurrentGameState = state;

            if(state == GameState.Menu && (prev == GameState.Playing || prev == GameState.Loss || prev == GameState.Win))
            {
                SFXManager.Instance.PlayMusic(SFXManager.Instance.MenuSong);
                Map.ClearGameMap();
            }

            if (state == GameState.Loss && (prev == GameState.Playing || prev == GameState.Win))
            {
                SFXManager.Instance.StopMusic();
                SFXManager.Instance.PlayMusic(SFXManager.Instance.FailureSong);
            }
        }
    }
}
