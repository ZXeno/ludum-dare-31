﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace LD31
{
    class PauseAndLoss
    {
        private Game1 _game;

        private Texture2D _buttonBG;
        private SpriteFont _font;

        private ListGroup _pauseMenuGroup;
        private ListGroup _lossMenuGroup;
        private Button _resumeButton;
        private Button _exitButton;
        private Button _mainMenu;
        private Label _menuLabel;
        
        private List<IControl> _ctrlList;

        public PauseAndLoss(Game1 game, SpriteFont font, Texture2D buttonbg)
        {
            _game = game;
            _ctrlList = new List<IControl>();
            _font = font;
            _buttonBG = buttonbg;
        }

        public void LoadContent()
        {
            _pauseMenuGroup = new ListGroup(new Point(_game.Graphics.PreferredBackBufferWidth / 2 - _buttonBG.Width / 2, _game.Graphics.PreferredBackBufferHeight / 2), _buttonBG.Width);
            _lossMenuGroup = new ListGroup(new Point(_game.Graphics.PreferredBackBufferWidth / 2 - _buttonBG.Width / 2, _game.Graphics.PreferredBackBufferHeight / 2), _buttonBG.Width);
            _menuLabel = new Label(_game, _game.Content.Load<SpriteFont>("titlefont"), "", new Point(_game.Graphics.PreferredBackBufferWidth / 2 - 125, _game.Graphics.PreferredBackBufferHeight / 4));

            _resumeButton = new Button(_game, _buttonBG, Point.Zero, _font);
            _exitButton = new Button(_game, _buttonBG, Point.Zero, _font);
            _mainMenu = new Button(_game, _buttonBG, Point.Zero, _font);

            Action<Button> resume = new Action<Button>(delegate { _game.ChangeGameState(GameState.Playing); });
            Action<Button> exit = new Action<Button>(delegate { _game.Exit(); });
            Action<Button> menu = new Action<Button>(delegate { _game.ChangeGameState(GameState.Menu); });

            _resumeButton.D = resume;
            _resumeButton.SetText("RESUME GAME");
            _exitButton.D = exit;
            _exitButton.SetText("EXIT GAME");
            _mainMenu.D = menu;
            _mainMenu.SetText("MAIN MENU");
            

            _pauseMenuGroup.AddControl(_resumeButton);
            _pauseMenuGroup.AddControl(_mainMenu);
            _pauseMenuGroup.AddControl(_exitButton);
            _pauseMenuGroup.LineHeight = 24;

            _lossMenuGroup.AddControl(_mainMenu);
            _lossMenuGroup.AddControl(_exitButton);
            _lossMenuGroup.LineHeight = 24;

            _ctrlList.Add(_pauseMenuGroup);
            _ctrlList.Add(_lossMenuGroup);
        }

        public void Update()
        {
            if (_game.CurrentGameState == GameState.Loss || _game.CurrentGameState == GameState.Pause || _game.CurrentGameState == GameState.Win)
            {
                if(_game.CurrentGameState == GameState.Loss)
                {
                    if(Player.Current.Money <= 0)
                    {
                        _menuLabel.Text = "YOU HAVE RUN OUT OF MONEY!";
                    }
                    if(Player.Current.OwnedShips.Count <= 0)
                    {
                        _menuLabel.Text = "YOU HAVE NO MORE SHIPS!";
                    }
                    _lossMenuGroup.HandleInput();
                    _lossMenuGroup.Update();
                    _lossMenuGroup.Reposition();
                }

                if(_game.CurrentGameState == GameState.Pause)
                {
                    _menuLabel.Text = "PAUSED";
                    _pauseMenuGroup.HandleInput();
                    _pauseMenuGroup.Update();
                    _pauseMenuGroup.Reposition();
                }

                _menuLabel.SetPosition(new Point(_game.Graphics.PreferredBackBufferWidth / 2 - Convert.ToInt32(_menuLabel.Font.MeasureString(_menuLabel.Text).X / 2), _game.Graphics.PreferredBackBufferHeight / 4));
            }
        }

        public void Draw()
        {
            if (_game.CurrentGameState == GameState.Loss || _game.CurrentGameState == GameState.Pause || _game.CurrentGameState == GameState.Win)
            {
                _menuLabel.Draw();
                if (_game.CurrentGameState == GameState.Loss)
                {
                    _lossMenuGroup.Draw();
                }

                if (_game.CurrentGameState == GameState.Pause)
                {
                    _pauseMenuGroup.Draw();
                }
            }
        }
    }
}
